/*
 * GPIO.h
 *
 *  Created on: 27 Oct 2018
 *      Author: Johannes
 */

#ifndef HAL_GPIO_H_
#define HAL_GPIO_H_

#include "hal.h"

#define PORT_LED_GREEN (PORT9)
#define PIN_LED_GREEN (7)
#define PORT_LED_RED (PORT1)
#define PIN_LED_RED (0)
#define BUTTON_S1_BIT (BIT1)
#define BUTTON_S2_BIT (BIT2)
#define PORT_BUTTON_S1 (PORT1)
#define PORT_BUTTON_S2 (PORT2)

typedef enum{
    PORT1,
    PORT2,
    PORT3,
    PORT4,
    PORT5,
    PORT6,
    PORT7,
    PORT8,
    PORT9,
    PORT10,
    PORT11,
    MAX_PORT
}GPIO_Port_t;

// Pin MUX
// MUXA General purpose I/O is selected
// MUXB Primary module function is selected
// MUXC Secondary module function is selected
// MUXD Tertiary module function is selected

typedef enum{
    MUXA,
    MUXB,
    MUXC,
    MUXD
}PinMux_t;

typedef enum{
    RISING_EDGE,
    FALLING_EDGE,
    BOTH_EDGES
}Edge_t;

typedef uint8_t Pin_t;

typedef enum{
    LOW,
    HIGH
}State_t;

typedef enum{
    INPUT,
    OUTPUT
}Direction_t;

typedef enum{
    PULLDOWN,
    PULLUP,
}Resistor_t;

typedef enum{
    ENABLE_INTERRUPT,
    DISABLE_INTERRUPT
}Interrupt_t;

typedef enum{
    DriveStrenght2mA,
    DriveStrenght4mA,
    DriveStrenght8mA
}DriveStrenght_t;

void gpio_init(void);
void gpio_setPortDirection(GPIO_Port_t gpio, Pin_t pin, Direction_t dir);
void gpio_setREN(GPIO_Port_t gpio, Pin_t pin, Resistor_t resistor);
void gpio_setInterruptEdgeSelct(GPIO_Port_t gpio, Edge_t edge);
void gpio_enableInterrupt(GPIO_Port_t gpio, Interrupt_t state);
void gpio_clearInterruptFlag(GPIO_Port_t gpio, Pin_t pin);
void gpio_setOutputType(GPIO_Port_t gpio, PinMux_t mux);
void gpio_setOutput(GPIO_Port_t gpio, Pin_t pin, State_t state);
void gpio_toggleOutput(GPIO_Port_t gpio, Pin_t pin);
void gpio_toggleInterruptEdgeSelect(GPIO_Port_t gpio, Pin_t pin);



#endif /* HAL_GPIO_H_ */
