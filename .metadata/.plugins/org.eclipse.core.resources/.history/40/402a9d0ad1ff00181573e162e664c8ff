/*
 * button.c
 *
 *  Created on: Oct 28, 2018
 *      Author: Johannes
 */

#include "button.h"

void button_init(Button_t button)
{
    switch (button)
    {
    case BUTTON1:
        button_setPortDirection(BUTTON1);
        button_setREN(BUTTON1, PULLUP);
        button_setOutput(BUTTON1, HIGH);
        button_clearButtonInterruptFlag(BUTTON1);
        break;
    case BUTTON2:
        button_setPortDirection(BUTTON2);
        button_setREN(BUTTON2, PULLUP);
        button_setOutput(BUTTON2, HIGH);
        button_clearButtonInterruptFlag(BUTTON2);
        break;
    default:
        break;
    }
}

void button_setOutput(Button_t button, State_t state){
    switch (button)
            {
            case BUTTON1:
                gpio_setOutput(GPIO_BUTTON_S1, state);
                break;
            case BUTTON2:
                gpio_setOutput(PORT_BUTTON_S2, state);
                break;
            default:
                break;
            }
}

void button_setREN(Button_t button, Resistor_t resistor){
    switch (button)
        {
        case BUTTON1:
            gpio_setREN(GPIO_BUTTON_S1, resistor);
            break;
        case BUTTON2:
            gpio_setREN(PORT_BUTTON_S2, resistor);
            break;
        default:
            break;
        }
}

void button_setPortDirection(Button_t button){
    switch (button)
       {
       case BUTTON1:
           gpio_setPortDirection(GPIO_BUTTON_S1, BIT1, INPUT);
           break;
       case BUTTON2:
           gpio_setPortDirection(PORT_BUTTON_S2, BIT2, INPUT);
           break;
       default:
           break;
       }
}

void button_enableInterrupt(Button_t button){
    switch (button)
       {
       case BUTTON1:
           gpio_enableInterrupt(GPIO_BUTTON_S1, ENABLE_INTERRUPT);
           break;
       case BUTTON2:
           gpio_enableInterrupt(PORT_BUTTON_S2, ENABLE_INTERRUPT);
           break;
       default:
           break;
       }
}

void button_setInterruptEdgeSelect(Button_t button){
    switch (button)
        {
        case BUTTON1:
            gpio_setInterruptEdgeSelct(GPIO_BUTTON_S1, RISING_EDGE);
            break;
        case BUTTON2:
            gpio_setInterruptEdgeSelct(PORT_BUTTON_S2, RISING_EDGE);
            break;
        default:
            break;
        }
}

void button_clearButtonInterruptFlag(Button_t button){
    switch (button)
    {
    case BUTTON1:
        gpio_clearInterruptFlag(GPIO_BUTTON_S1);
        break;
    case BUTTON2:
        gpio_clearInterruptFlag(PORT_BUTTON_S2);
    }
}

void button_toggleInterruptEdgeSelect(Button_t button){
    switch (button)
        {
        case BUTTON1:
            gpio_toggleInterruptEdgeSelect(GPIO_BUTTON_S1);
            break;
        case BUTTON2:
            gpio_toggleInterruptEdgeSelect(PORT_BUTTON_S2);
            break;
        default:
            break;
        }
}

bool button_isButtonPressed(Button_t button){
    // Attention this function reverses the logic of the buttons on the MSP430
    // the register of the button is zero when the button is pressed
    // however the function returns true when the button is pressed

    bool value = false;

    switch (button)
    {
    case BUTTON1:
        if ((P1IN & BUTTON_S1_BIT) == 0)
        {
            value = true;
        }
        else
        {
            value = false;
        }
        break;
    case BUTTON2:
        if ((P1IN & BUTTON_S2_BIT) == 0)
        {
            value = true;
        }
        else
        {
            value = false;
        }break;
    default:
        break;
    }
    return value;
}

