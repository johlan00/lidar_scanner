/*
 * led.h
 *
 *  Created on: Oct 28, 2018
 *      Author: Johannes
 */

#ifndef DRIVER_LED_H_
#define DRIVER_LED_H_

#include "../hal/hal.h"

typedef enum{
    LED_2,
    LED_1
}LED_t;

void led_init(LED_t led);
void led_setState(LED_t led, State_t state);
void led_toggleState(LED_t led);
void led_PWM(LED_t led);
void led_BlinkLed(LED_t led, unsigned int millis);
void led_turnOnForMillis(LED_t led, unsigned int millis);

#endif /* DRIVER_LED_H_ */
