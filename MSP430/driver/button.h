/*
 * button.h
 *
 *  Created on: Oct 28, 2018
 *      Author: Johannes
 */

#ifndef DRIVER_BUTTON_H_
#define DRIVER_BUTTON_H_

#include "../hal/GPIO.h"


typedef enum{
    BUTTON1,
    BUTTON2
}Button_t;

void button_init(Button_t button);
void button_clearButtonInterruptFlag(Button_t button);
bool button_isButtonPressed(Button_t button);
void button_setInterruptEdgeSelect(Button_t button);
void button_enableInterrupt(Button_t button);
void button_setPortDirection(Button_t button);
void button_setREN(Button_t button, Resistor_t resistor);
void button_toggleInterruptEdgeSelect(Button_t button);
void button_setOutput(Button_t button, State_t state);

#endif /* DRIVER_BUTTON_H_ */
