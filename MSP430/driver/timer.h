/*
 * timer.h
 *
 *  Created on: Nov 6, 2018
 *      Author: Johannes
 */

#ifndef DRIVER_TIMER_H_
#define DRIVER_TIMER_H_

#include "button.h"

uint16_t button_getTimePressed(void);
uint16_t button_getPauseTime(void);
void button_initTimer(void);

#endif /* DRIVER_TIMER_H_ */
