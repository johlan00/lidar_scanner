/*
 * TFmini.h
 *
 *  Created on: 26 Dec 2018
 *      Author: Johannes
 */

#ifndef DRIVER_TFMINI_H_
#define DRIVER_TFMINI_H_

#include "../hal/hal.h"

void TFMini_init(void);
uint16_t TFMini_getData();

#endif /* DRIVER_TFMINI_H_ */
