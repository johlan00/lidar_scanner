/*
 * timer.c
 *
 *  Created on: Nov 6, 2018
 *      Author: Johannes
 */

#include "timer.h"

#define DUTYCYCLE 250 // 25 %
#define COUNTLIMIT 1000

static uint16_t timePressed; // atomar access does not get interrupted
static uint16_t pauseTime;

#pragma vector = TIMER1_A0_VECTOR
__interrupt void TA_1_ISR(void)
{
    if(button_isButtonPressed(BUTTON2) == true){
        timePressed++;
        pauseTime = 0;
    }
    else{
        timePressed = 0;
        pauseTime++;
    }
    TA1CCTL0 &= ~CCIFG;
}

void button_initTimer(){
    timer_initTimer(TIMER2);
}

uint16_t button_getTimePressed(void){
    return timePressed;
}

uint16_t button_getPauseTime(void){
    return pauseTime;
}


