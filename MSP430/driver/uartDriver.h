/*
 * uartDriver.h
 *
 *  Created on: 14 Dec 2018
 *      Author: Johannes
 */

#ifndef DRIVER_UARTDRIVER_H_
#define DRIVER_UARTDRIVER_H_

#include "../hal/hal.h"

typedef enum
{
    WIRED1,
    WIRED2,
    BACKCHANNEL,
    BLUETOOTH
} MY_UART_t;

// UART0 - WIRED1
void uart0_init(void);
void uart0_writeByte(uint8_t byte);
void uart0_readByte(uint8_t *byte);
void uart0_writeString(uint8_t str[]);
void uart0_writeWord(uint16_t word);

// UART1 - BACKCHANNEL
void uart1_init(void);
void uart1_writeByte(uint8_t byte);
void uart1_readByte(uint8_t *byte);
void uart1_writeString(const uint8_t str[]);
void uart1_writeWord(uint16_t word);


#endif /* DRIVER_UARTDRIVER_H_ */
