/*
 * servo.c
 *
 *  Created on: 28 Dec 2018
 *      Author: Johannes
 */

#include "servo.h"

#define DEFAULT_MICRO_S (0)
#define SERVO_PORT (PORT2)
#define HORIZONTAL_PORT (6)
#define VERTICAL_PORT (7)
#define DUTY_CYCLE_PERIOD_REG_V (20000)
#define DUTY_CYCLE_MAX (20000)
#define DEGREE_MAX (180)
#define DEGREE_MIN (0)
#define VERTICAL_CR_LOW (0.023) // Correction Factor
#define VERTICAL_CR_HIGH (0.12)
#define HORIZONTAL_CR_LOW (0.125)
#define HORIZONTAL_CR_HIGH (0.035)

// map - maps a certain range to a desired range e.g.from 1 - 1000 to 0 % - 100 %
static uint16_t map(uint32_t value, uint32_t in_min, uint32_t in_max, uint32_t out_min, uint32_t out_max){
    uint32_t calc = (value - in_min)*((float)(out_max - out_min)/(float)(in_max - in_min)) + out_min;
    return (uint16_t) calc;
}

void servo_H_init(){
    timer_initPwmTimer(TIMER3, DUTY_CYCLE_PERIOD_REG_V, DEFAULT_MICRO_S);
    gpio_setPortDirection(SERVO_PORT, HORIZONTAL_PORT, OUTPUT);
    gpio_setPinMux(SERVO_PORT, HORIZONTAL_PORT, MUXB);
}

void servo_V_init(){
    timer_initPwmTimer(TIMER3, DUTY_CYCLE_PERIOD_REG_V, DEFAULT_MICRO_S);
    gpio_setPortDirection(SERVO_PORT, VERTICAL_PORT, OUTPUT);
    gpio_setPinMux(SERVO_PORT, VERTICAL_PORT, MUXB);
}

void servo_H_setDutyCycle(uint16_t dutyCycleRegValue){
    timer_setPwMDutyCycle(TIMER3, TIMER3_REG5, dutyCycleRegValue);
}

void servo_V_setDutyCycle(uint16_t dutyCycleRegValue){
    timer_setPwMDutyCycle(TIMER3, TIMER3_REG6, dutyCycleRegValue);
}

void servo_V_setPosition(uint16_t degree){
    uint16_t dutyCycleRegValue = map(degree, DEGREE_MIN, DEGREE_MAX, DUTY_CYCLE_MAX*VERTICAL_CR_LOW, DUTY_CYCLE_MAX*VERTICAL_CR_HIGH);
    servo_V_setDutyCycle(dutyCycleRegValue);
}

void servo_H_setPosition(uint16_t degree){
    uint16_t dutyCycleRegValue = map(degree, DEGREE_MIN, DEGREE_MAX, DUTY_CYCLE_MAX*HORIZONTAL_CR_HIGH, DUTY_CYCLE_MAX*HORIZONTAL_CR_LOW);
    servo_H_setDutyCycle(dutyCycleRegValue);
}

