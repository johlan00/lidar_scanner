/*
 * servo.h
 *
 *  Created on: 28 Dec 2018
 *      Author: Johannes
 */

#ifndef DRIVER_SERVO_H_
#define DRIVER_SERVO_H_

#include "../hal/hal.h"
#include "../hal/gpio.h"

void servo_H_init(void);
void servo_V_init(void);
void servo_H_setDutyCycle(uint16_t microSeconds);
void servo_V_setDutyCycle(uint16_t microSeconds);
void servo_V_setPosition(uint16_t degree);
void servo_H_setPosition(uint16_t degree);


#endif /* DRIVER_SERVO_H_ */

