/*
 * uartDriver.c
 *
 *  Created on: 14 Dec 2018
 *      Author: Johannes
 */

#include "../hal/hal.h"

void uart0_init()
{
    uart_init(UART0);
}

void uart0_writeByte(uint8_t byte)
{
    uart_writeByte(UART0, byte);
}

void uart0_writeString(const uint8_t str[]){
    uart_writeString(UART0, str);
}


void uart0_readByte(uint8_t *byte)
{
    uart_readByte(UART0, byte);
}

void uart0_writeWord(uint16_t word){
    uart_writeWord(UART0, word);
}

void uart1_init()
{
    uart_init(UART1);
}

void uart1_writeByte(uint8_t byte)
{
    uart_writeByte(UART1, byte);
}

void uart1_writeString(const uint8_t str[]){
    uart_writeString(UART1, str);
}

void uart1_readByte(uint8_t *byte)
{
    uart_readByte(UART1, byte);
}

void uart1_writeWord(uint16_t word){
    uart_writeWord(UART1, word);
}
