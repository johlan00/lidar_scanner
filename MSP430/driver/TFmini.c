/*
 * TFmini.c
 *
 *  Created on: 26 Dec 2018
 *      Author: Johannes
 */


#include "TFmini.h"
#include "../driver/uartDriver.h"

#define DATA_N_BYTES (9) // Amount of bytes transmitted by the TFMini during one measurement
#define DISTANCE_LOW_BYTE (2)
#define DISTANCE_HIGH_BYTE (3)
#define STRENGTH_LOW_BYTE (4)
#define STRENGTH_HIGH_BYTE (5)
#define HEADER (0x59)

/*
 * Following UART stream provides TF Lidar mini sensor
 * The checksum is calculated by accumulating the bytes from 0 to 7
 * Byte 6 holds the mode information, respectively the range: short or long range
 *
 *
 *|-------||-------||----------||----------||----------||----------||------------||----------||----------|
 *| Byte0 || Byte1 ||   Byte2  ||   Byte3  ||   Byte4  ||   Byte5  ||    Byte6   ||   Byte7  ||   Byte8  |
 *|-------||-------||----------||----------||----------||----------||------------||----------||----------|
 *| Header|| Header||  L_Byte  ||  H_Byte  ||  L_Byte  ||  H_Byte  ||    MODE    ||   Spare  ||  CHKSUM  |
 *| 0x59  || 0x59  || Distance || Distance || Strength || Strength || LONG/SHORT ||   Byte   ||          |
 *|-------||-------||----------||----------||----------||----------||------------||----------||----------|
 *
 *
 */

void TFMini_init(){
    uart0_init();
}

uint16_t TFMini_getData(){
    uint8_t byte = 0;
    uint8_t byte2 = 0;
    uint8_t data[(DATA_N_BYTES)+1];
    uint16_t checksum = 0;
    uint16_t distance;

    // wait to begin stream at the right position
    while((byte & byte2) != HEADER){
        uart0_readByte(&byte);
        uart0_readByte(&byte2);
    }
    // store header information for checksum calculation
    data[0] = byte;
    data[1] = byte2;

    // retrieve byte2 - byte 7
    for(unsigned int i = 2; i <= (DATA_N_BYTES-1); i++){
        uart0_readByte(&byte);
        data[i] = byte;
    }

    checksum = data[0] + data[1] + data[2] + data[3] + data[4] + data[5] + data[6] + data[7];

    if((checksum & 0xFF) == data[(DATA_N_BYTES-1)]){
        distance = ((data[3] << 8) | data[2]);
        return distance;
    }
    // return zero when checksum is not valid
    return 0;
}

