/*
 * led.c
 *
 *  Created on: Oct 28, 2018
 *      Author: Johannes
 */
#include "led.h"

void led_init(LED_t led)
{
    switch (led)
    {
    case LED_1:
        gpio_setPortDirection(PORT_LED_1, PIN_LED_1, OUTPUT);
        gpio_setPinMux(PORT_LED_1, PIN_LED_1, MUXA);
        gpio_setOutput(PORT_LED_1, PIN_LED_1, LOW);
        break;
    case LED_2:
        gpio_setPortDirection(PORT_LED_2, PIN_LED_2, OUTPUT);
        gpio_setPinMux(PORT_LED_2, PIN_LED_2, MUXA);
        gpio_setOutput(PORT_LED_2, PIN_LED_2, LOW);
        break;
    default:
        break;
    }
}

void led_setState(LED_t led, State_t state)
{
    switch (led)
    {
    case LED_1:
        gpio_setOutput(PORT_LED_1, PIN_LED_1, state);
        break;

    case LED_2:
        gpio_setOutput(PORT_LED_2, PIN_LED_2, state);
        break;
    default:
        break;
    }
}

void led_turnOnForMillis(LED_t led, unsigned int millis)
{

    switch (led)
    {
    case LED_1:
        gpio_setOutput(PORT_LED_1, PIN_LED_1, HIGH);
        system_sleep(millis);
        gpio_setOutput(PORT_LED_1, PIN_LED_1, LOW);
        break;

    case LED_2:
        gpio_setOutput(PORT_LED_2, PIN_LED_2, HIGH);
        system_sleep(millis);
        gpio_setOutput(PORT_LED_2, PIN_LED_2, LOW);
        break;
    default:
        break;
    }
}

void led_toggleState(LED_t led)
{
    switch (led)
    {
    case LED_1:
        gpio_toggleOutput(PORT_LED_1, PIN_LED_1);
        break;

    case LED_2:
        gpio_toggleOutput(PORT_LED_2, PIN_LED_2);
        break;

    default:
        break;
    }
}

void led_BlinkLed(LED_t led, unsigned int pauseTime)
{
    led_toggleState(led);
    system_sleep(pauseTime);
    led_toggleState(led);
    system_sleep(pauseTime);
}
