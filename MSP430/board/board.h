/*
 * board.h
 *
 *  Created on: Oct 31, 2018
 *      Author: Johannes
 */

#ifndef HAL_BOARD_BOARD_H_
#define HAL_BOARD_BOARD_H_

#include "EXPMSP430FR6989.h"


// LED
// LED GREEN
#define PORT_LED_1 (PORT9)
#define PIN_LED_1 (7)

// LED RED
#define PORT_LED_2 (PORT1)
#define PIN_LED_2 (0)

// BUTTONS
#define PIN_BUTTON_S1 (BIT1)
#define PIN_BUTTON_S2 (BIT2)
#define PORT_BUTTON_S1 (PORT1)
#define PORT_BUTTON_S2 (PORT2)

// I2C
// SDA PORT1 PIN6 | SCL PORT1 PIN7
#define I2C_0_PORT (PORT1)
#define I2C_0_SDA_PIN (6)
#define I2C_0_SCL_PIN (7)

// UART
#define UART0_PORT (PORT4)
#define UART1_PORT (PORT3)
#define UART0_TXD_PIN (2)
#define UART0_RXD_PIN (3)
#define UART1_TXD_PIN (4)
#define UART1_RXD_PIN (5)

void board_init();

#endif /* HAL_BOARD_BOARD_H_ */
