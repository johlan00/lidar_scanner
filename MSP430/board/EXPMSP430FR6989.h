/*
 * EXPMSP430FR6989.h
 *
 *  Created on: Oct 31, 2018
 *      Author: Johannes
 */

#ifndef HAL_BOARD_EXPMSP430FR6989_H_
#define HAL_BOARD_EXPMSP430FR6989_H_

#include <msp430.h>
#include "../hal/gpio.h"
#include "../hal/timer.h"

void EXPMSP430FR6989_init();

#endif /* HAL_BOARD_EXPMSP430FR6989_H_ */
