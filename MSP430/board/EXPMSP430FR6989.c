/*
 * EXPMSP430FR6989.c
 *
 *  Created on: Oct 31, 2018
 *      Author: Johannes
 */

#include "EXPMSP430FR6989.h"

void EXPMSP430FR6989_init(){
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer
    PM5CTL0 &= ~LOCKLPM5;
    gpio_init();
}
