/*
 * apps.h
 *
 *  Created on: 27 Oct 2018
 *      Author: Johannes
 */

#ifndef APPS_APPS_H_
#define APPS_APPS_H_

#include <math.h>
#include "../driver/led.h"
#include "../driver/button.h"
#include "../driver/uartDriver.h"
#include "../driver/servo.h"
#include "../driver/TFmini.h"
#include "../driver/timer.h"
#include "../board/board.h"
#include <stdio.h>

typedef enum LidarStateMachine{
    S_IDLE,
    S_MEASUREMENT
}LidarStateMachine_t;

void app_startButtonInterruptExample(void);

void app_flushString(char string[]);
void app_terminal(MY_UART_t uart, char string[]);
void app_listenTerminal(MY_UART_t uart, char string[]);
void app_initUart(MY_UART_t uart);
void app_writeString(MY_UART_t uart, char string[]);
void app_LidarInit();
void app_Lidar();


#endif /* APPS_APPS_H_ */
