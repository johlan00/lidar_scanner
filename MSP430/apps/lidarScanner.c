/*
 * lidarScanner.c
 *
 *  Created on: 9 Feb 2019
 *      Author: Johannes
 */

#include "apps.h"

// servo defines in degree - decimal
#define VERT_DEF_POS (90)
#define VERT_MAX_POS (50)
#define HORZ_DEF_POS (0)
#define HORZ_RIGHT_POS (40)
#define HORZ_LEFT_POS (120)

// servo control
#define STEP_SIZE (1) // in degree
#define DELAY (0) // in millis

#define LEFT_DIRECTION (0) // bool
#define RIGHT_DIRECTION (1) // bool

// ASCII signs
#define SEPARATION_SIGN (44)
#define NEW_LINE (10)
#define START_BYTE (0x30)
#define STOP_BYTE (0x32)
#define CARRIAGE_RETURN (0xD)
#define ASCII_OFFSET (0x30)
#define START_BYTE_1 (0x31)
#define START_BYTE_2 (0x30)
#define STOP_BYTE_1 (0x31)
#define STOP_BYTE_2 (0x31)

#define BUTTON_TIME_PRESSED (500) // in millis

static LidarStateMachine_t appState;
static void lidar_ObtainCoordinates();
static void lidar_sendDistanceAsDigits(uint16_t distance);
static void lidar_sendData(uint8_t elevation, uint8_t azimuth);
static void lidar_sendAngleAsDigits(uint8_t distance);
static void lidar_sendStartOfMeasurementFrame();
static void lidar_sendMeausrementStop();
static void lidar_setDefaultPosition();

void app_LidarInit(){
    EXPMSP430FR6989_init();
    system_systickInit();
    button_initTimer();
    button_init(BUTTON1);
    interrupt_enableGlobalInterrupt();
    uart0_init(); // WIRED UART
    uart1_init(); // Backchannel UART
    servo_H_init();
    servo_V_init();
    // set servo default position
    servo_H_setPosition(HORZ_DEF_POS);
    servo_V_setPosition(VERT_DEF_POS);
}

void app_Lidar(){
    appState = S_IDLE;

    if(button_getTimePressed() >= BUTTON_TIME_PRESSED){
        appState = S_MEASUREMENT;
    }

    switch(appState){
    case S_IDLE:
        break;
    case S_MEASUREMENT:
        lidar_ObtainCoordinates();
        break;
    default:
        break;

    }
}

static void lidar_ObtainCoordinates(){
    static bool direction;
    lidar_setDefaultPosition();
    direction = RIGHT_DIRECTION;

    for(int elevation = VERT_DEF_POS; elevation >= VERT_MAX_POS; elevation = elevation - STEP_SIZE){
        servo_V_setPosition((uint8_t)elevation);
        if(direction == RIGHT_DIRECTION){
            for(uint8_t azimuth = HORZ_RIGHT_POS; azimuth <= HORZ_LEFT_POS; azimuth = azimuth + STEP_SIZE){
                servo_H_setPosition(azimuth);
                system_sleep(DELAY);
                lidar_sendData((uint8_t)elevation, azimuth);

            }
            direction = LEFT_DIRECTION;
        }
        else{
            for(int azimuth = HORZ_LEFT_POS; azimuth >= HORZ_RIGHT_POS; azimuth = azimuth - STEP_SIZE){
                servo_H_setPosition(azimuth);
                system_sleep(DELAY);
                lidar_sendData((uint8_t)elevation, (uint8_t) azimuth);
            }
            direction = RIGHT_DIRECTION;
        }
    }

    lidar_sendMeausrementStop();
    lidar_setDefaultPosition();
    appState = S_IDLE;
    return;
}

static void lidar_sendData(uint8_t elevation, uint8_t azimuth){
    uint16_t distance = TFMini_getData();

    if(distance != 0){
        lidar_sendStartOfMeasurementFrame();
        lidar_sendDistanceAsDigits(distance);
        lidar_sendAngleAsDigits(elevation);
        lidar_sendAngleAsDigits(azimuth);
    }
}

static void lidar_sendDistanceAsDigits(uint16_t distance){
     uint16_t d4 = distance % 10;
     uint16_t d3 = distance / 10 % 10;
     uint16_t d2 = distance / 100 % 10;
     uint16_t d1 = distance / 1000 % 10;

     d4 = d4 + ASCII_OFFSET;
     d3 = d3 + ASCII_OFFSET;
     d2 = d2 + ASCII_OFFSET;
     d1 = d1 + ASCII_OFFSET;

     uart1_writeByte(d1);
     uart1_writeByte(NEW_LINE);
     uart1_writeByte(CARRIAGE_RETURN);
     uart1_writeByte(d2);
     uart1_writeByte(NEW_LINE);
     uart1_writeByte(CARRIAGE_RETURN);
     uart1_writeByte(d3);
     uart1_writeByte(NEW_LINE);
     uart1_writeByte(CARRIAGE_RETURN);
     uart1_writeByte(d4);
     uart1_writeByte(NEW_LINE);
     uart1_writeByte(CARRIAGE_RETURN);
}

static void lidar_sendAngleAsDigits(uint8_t distance){
     uint8_t d3 = distance % 10;
     uint8_t d2 = distance / 10 % 10;
     uint8_t d1 = distance / 100 % 10;

     d3 = d3 + ASCII_OFFSET;
     d2 = d2 + ASCII_OFFSET;
     d1 = d1 + ASCII_OFFSET;

     uart1_writeByte(d1);
     uart1_writeByte(NEW_LINE);
     uart1_writeByte(CARRIAGE_RETURN);
     uart1_writeByte(d2);
     uart1_writeByte(NEW_LINE);
     uart1_writeByte(CARRIAGE_RETURN);
     uart1_writeByte(d3);
     uart1_writeByte(NEW_LINE);
     uart1_writeByte(CARRIAGE_RETURN);
}

static void lidar_sendStartOfMeasurementFrame(){
    uart1_writeByte(START_BYTE_1);
    uart1_writeByte(START_BYTE_2);
    uart1_writeByte(NEW_LINE);
    uart1_writeByte(CARRIAGE_RETURN);
}

static void lidar_sendMeausrementStop(){
    uart1_writeByte(STOP_BYTE_1);
    uart1_writeByte(STOP_BYTE_2);
    uart1_writeByte(NEW_LINE);
    uart1_writeByte(CARRIAGE_RETURN);
}

static void lidar_setDefaultPosition(){
    servo_H_setPosition(HORZ_DEF_POS);
    servo_V_setPosition(VERT_DEF_POS);
}
