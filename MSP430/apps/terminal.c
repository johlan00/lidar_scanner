/*
 * terminal.c
 *
 *  Created on: 14 Dec 2018
 *      Author: Johannes
 */

#include <string.h>
#include "apps.h"

#define STRINGSIZE      (256)

void app_flushString(char string[])
{
    for (int i = 0; i <= STRINGSIZE; i++)
    {
        string[i] = '\0';
    }
}

void app_terminal(MY_UART_t uart, char string[])
{
    app_writeString(uart, "MSP430 ¢ ");
    app_flushString(string);
}

void app_writeString(MY_UART_t uart, char string[])
{
    switch (uart)
    {
    case WIRED1:
        uart0_writeString((uint8_t*)string);
        break;
    case BACKCHANNEL:
        uart1_writeString((uint8_t*)string);
        break;
    default:
        break;
    }
}

void app_commandTerminal(MY_UART_t uart, char string[])
{

    if (strncmp(string, "Test", 10) == 0)
    {
        app_writeString(BACKCHANNEL, "Test received");
    }
}

void app_listenTerminal(MY_UART_t uart, char string[])
{
    uint8_t input = 127;
    uint8_t i = 0;

    app_flushString(string);
    app_terminal(uart, string);

    switch (uart)
    {
    case WIRED1:
        do
        {
            uart0_readByte(&input);
            if(input != 127){
                uart0_writeByte(input);

            string[i] = (char) input;
            i++;
            }
        }
        while (input != '\n' && i <= STRINGSIZE);
        break;

    case BACKCHANNEL:
        do
        {
            uart1_readByte(&input);
            if(input != 127){
                uart1_writeByte(input);
                input = 127;
                string[i] = (char) input;
                i++;
            }
        }
        while (input != '\n' && input != '\r' && i <= STRINGSIZE);
        break;

    default:
        break;
    }

    app_commandTerminal(uart, string);

}

void app_initUart(MY_UART_t uart)
{

    switch (uart)
    {
    case WIRED1:
        uart0_init();
        app_writeString(uart, "MSP430 Terminal\n");
        break;

    case BACKCHANNEL:
        uart1_init();
        app_writeString(uart, "MSP430 Terminal\n");
        break;

    default:
        break;
    }
}
