/*
 * interrupt.h
 *
 *  Created on: Nov 10, 2018
 *      Author: Johannes
 */

#ifndef HAL_INTERRUPT_H_
#define HAL_INTERRUPT_H_

void interrupt_enableGlobalInterrupt(void);

#define __istate_ts unsigned short



#endif /* HAL_INTERRUPT_H_ */
