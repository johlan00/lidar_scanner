/*
 * timer.h
 *
 *  Created on: Nov 6, 2018
 *      Author: Johannes
 */

#ifndef HAL_MSP430_TIMER_H_
#define HAL_MSP430_TIMER_H_

#include "board/board.h"
#include "hal.h"

typedef enum{
    TIMER1,
    TIMER2,
    TIMER3,
    TIMER4
}Timer_t;

typedef enum{
    TIMER3_REG0,
    TIMER3_REG1,
    TIMER3_REG2,
    TIMER3_REG3,
    TIMER3_REG4,
    TIMER3_REG5,
    TIMER3_REG6
}TimerReg_t;

int timer_initTimer(Timer_t timer);
int timer_initPwmTimer(Timer_t timer, uint16_t periodRegValue ,uint16_t dutyCycleRegValue);
int timer_setPwMDutyCycle(Timer_t timer, TimerReg_t reg, uint16_t dutyCycleRegValue);


#endif /* HAL_MSP430_TIMER_H_ */
