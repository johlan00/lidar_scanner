/*
 * GPIO.h
 *
 *  Created on: 27 Oct 2018
 *      Author: Johannes
 */

#ifndef HAL_GPIO_H_
#define HAL_GPIO_H_

#include "hal.h"

typedef enum{
    PORT1,
    PORT2,
    PORT3,
    PORT4,
    PORT5,
    PORT6,
    PORT7,
    PORT8,
    PORT9,
    PORT10,
    PORT11,
    MAX_PORT
}GPIO_Port_t;

// Pin MUX
// MUXA General purpose I/O is selected
// MUXB Primary module function is selected
// MUXC Secondary module function is selected
// MUXD Tertiary module function is selected

typedef enum{
    MUXA,
    MUXB,
    MUXC,
    MUXD
}PinMux_t;

typedef enum{
    RISING_EDGE,
    FALLING_EDGE,
    BOTH_EDGES
}Edge_t;

typedef uint8_t Pin_t;

typedef enum{
    LOW,
    HIGH
}State_t;

typedef enum{
    INPUT,
    OUTPUT
}Direction_t;

typedef enum{
    PULLDOWN,
    PULLUP,
}Resistor_t;

typedef enum{
    ENABLE_INTERRUPT,
    DISABLE_INTERRUPT
}Interrupt_t;

typedef enum{
    DriveStrenght2mA,
    DriveStrenght4mA,
    DriveStrenght8mA
}DriveStrenght_t;

void gpio_init(void);
void gpio_setPortDirection(GPIO_Port_t gpio, Pin_t pin, Direction_t dir);
void gpio_setREN(GPIO_Port_t gpio, Pin_t pin, Resistor_t resistor);
void gpio_setInterruptEdgeSelct(GPIO_Port_t gpio, Pin_t pin, Edge_t edge);
void gpio_enableInterrupt(GPIO_Port_t gpio, Pin_t pin, Interrupt_t state);
void gpio_clearInterruptFlag(GPIO_Port_t gpio, Pin_t pin);
void gpio_setPinMux(GPIO_Port_t gpio, Pin_t pin ,PinMux_t mux);
void gpio_setOutput(GPIO_Port_t gpio, Pin_t pin, State_t state);
void gpio_toggleOutput(GPIO_Port_t gpio, Pin_t pin);
void gpio_toggleInterruptEdgeSelect(GPIO_Port_t gpio, Pin_t pin);

#endif /* HAL_GPIO_H_ */
