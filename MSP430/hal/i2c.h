/*
 * i2c.h
 *
 *  Created on: 12 Dec 2018
 *      Author: Johannes
 */

#ifndef HAL_I2C_H_
#define HAL_I2C_H_

#include "hal.h"

typedef enum{
    I2C_0,
    I2C_1
}I2C_t;

typedef enum{
    TRANSMIT,
    RECEIVE
}Mode_t;


int i2c_init(I2C_t bus, uint8_t slaveAddr);
int i2c_writeByte(I2C_t bus, uint8_t regAddr, uint8_t writeValue);
int i2c_readByte(I2C_t bus,  uint8_t regAddr, uint8_t *outValue);
int i2c_readWord(I2C_t bus, uint8_t regAddr, uint16_t *outValue);
int i2c_writeWord(I2C_t bus, uint8_t slaveAddr, uint16_t writeValue);
void i2c_setSlaveAddress(uint8_t slaveAddress);
void i2c_setMode(Mode_t i2cMode);
bool i2c_txBufIsEmpty(I2C_t bus);
bool i2c_rxHasByte(I2C_t bus);
void i2c_waitForStart();
void i2c_waitForStop();
void i2c_MasterSendStart();
void i2c_MasterSendStop();
void i2c_setMode(Mode_t i2cMode);
void i2c_setSlaveAddress(uint8_t slaveAddress);


#endif /* HAL_I2C_H_ */
