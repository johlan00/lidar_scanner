/*
 * timer.c
 *
 *  Created on: Nov 6, 2018
 *      Author: Johannes
 */

#include "../timer.h"
#include "stdint.h"

#define COUNTLIMIT (1000)

int timer_initTimer(Timer_t timer)
{
    int value = -1;
    switch (timer)
    {
    case TIMER1:
        TA0CCR0 = COUNTLIMIT;
        TA0CCTL0 = CCIE;
        TA0CTL = TASSEL_2 | ID_0 | MC_1 | TACLR; // TASSEL_2 = sub-system master clock
        TA0CCTL0 &= ~CCIFG;
        value = 0;
        break;

    case TIMER2:
        TA1CCR0 = COUNTLIMIT;
        TA1CCTL0 = CCIE;
        TA1CTL = TASSEL_2 | ID_0 | MC_1 | TACLR;
        TA1CCTL0 &= ~CCIFG;
        value = 0;
        break;

    default:
        value = -1;
        break;
    }
    return value;
}

int timer_initPwmTimer(Timer_t timer, uint16_t periodRegValue, uint16_t dutyCycleRegValue)
{
    //Attention in this case the input divider ID_3 is used
    int value = -1;
    switch (timer)
    {
    case TIMER3:
        TB0CCR0 = periodRegValue;
        TB0CCR1 = dutyCycleRegValue;
        TB0CCR5 = dutyCycleRegValue;
        TB0CCR6 = dutyCycleRegValue;
        TB0CCTL1 = OUTMOD_7;
        TB0CTL = TBSSEL_2 | MC_1 | TBCLR; // tassel = clock source select, id = input divider (further divider by tadiex) mc = mode control
        TB0CCTL6 = TBSSEL_2 | ID_3;
        TB0CCTL5 = TBSSEL_2 | ID_3;
        value = 0;
        break;
    default:
        value = -1;;
        break;
    }
    return value;
}


int timer_setPwMDutyCycle(Timer_t timer, TimerReg_t reg, uint16_t dutyCycleRegValue)
{
    int value = -1;
    switch (timer)
    {
    //Todo implement completely
    case TIMER3:
        switch (reg)
        {
        case TIMER3_REG5:
            TB0CCTL0 &= ~CCIFG; // Clear stale indicator
            while (!(TB0CCTL0 & CCIFG)); // Wait until Up mode cycle completes
            TB0CCR5 = dutyCycleRegValue;
            value = 0;
            break;
        case TIMER3_REG6:
            TB0CCTL0 &= ~CCIFG; // Clear stale indicator
            while (!(TB0CCTL0 & CCIFG)); // Wait until Up mode cycle completes
            TB0CCR6 = dutyCycleRegValue;
            value = 0;
            break;
        default:
            value = -1;
            break;
        }
        break;

        default:
            value = -1;
            break;
    }
    return value;
}
