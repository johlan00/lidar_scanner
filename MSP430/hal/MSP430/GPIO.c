/*
 * GPIO.c
 *
 *  Created on: 27 Oct 2018
 *      Author: Johannes
 */

#include "../GPIO.h"

#define BIT_16_MASK 0xFFFF

void gpio_setPortDirection(GPIO_Port_t gpio, Pin_t pin, Direction_t dir)
{
    switch (gpio)
    {
    case PORT1:
        if (dir == OUTPUT)
        {
            P1DIR |= (1 << pin);
        }
        else
        {
            P1DIR &= ~(1 << pin);
        }
        break;
    case PORT2:
        if (dir == OUTPUT)
        {
            P2DIR |= (1 << pin);
        }
        else
        {
            P2DIR &= ~(1 << pin);
        }
        break;
    case PORT3:
        if (dir == OUTPUT)
        {
            P3DIR |= (1 << pin);
        }
        else
        {
            P3DIR &= ~(1 << pin);
        }
        break;
    case PORT4:
        if (dir == OUTPUT)
        {
            P4DIR |= (1 << pin);
        }
        else
        {
            P4DIR &= ~(1 << pin);
        }
        break;
    case PORT5:
        if (dir == OUTPUT)
        {
            P5DIR |= (1 << pin);
        }
        else
        {
            P5DIR &= ~(1 << pin);
        }
        break;
    case PORT6:
        if (dir == OUTPUT)
        {
            P6DIR |= (1 << pin);
        }
        else
        {
            P6DIR &= ~(1 << pin);
        }
        break;
    case PORT7:
        if (dir == OUTPUT)
        {
            P7DIR |= (1 << pin);
        }
        else
        {
            P7DIR &= ~(1 << pin);
        }
        break;
    case PORT8:
        if (dir == OUTPUT)
        {
            P8DIR |= (1 << pin);
        }
        else
        {
            P8DIR &= ~(1 << pin);
        }
        break;
    case PORT9:
        if (dir == OUTPUT)
        {
            P9DIR |= (1 << pin);
        }
        else
        {
            P9DIR &= ~(1 << pin);
        }
        break;
    default:
        break;
    }
}

void gpio_setPinMux(GPIO_Port_t gpio, Pin_t pin, PinMux_t mux)
{
    switch (gpio)
    {
    case PORT1:
        switch (mux)
        {
        case MUXA:
            P1SEL0 &= ~(1 << pin);
            P1SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P1SEL0 |= (1 << pin);
            P1SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P1SEL0 &= ~(1 << pin);
            P1SEL1 |= (1 << pin);
            break;
        case MUXD:
            P1SEL0 |= (1 << pin);
            P1SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT2:
        switch (mux)
        {
        case MUXA:
            P2SEL0 &= ~(1 << pin);
            P2SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P2SEL0 |= (1 << pin);
            P2SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P2SEL0 &= ~(1 << pin);
            P2SEL1 |= (1 << pin);
            break;
        case MUXD:
            P2SEL0 |= (1 << pin);
            P2SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT3:
        switch (mux)
        {
        case MUXA:
            P3SEL0 &= ~(1 << pin);
            P3SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P3SEL0 |= (1 << pin);
            P3SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P3SEL0 &= ~(1 << pin);
            P3SEL1 |= (1 << pin);
            break;
        case MUXD:
            P3SEL0 |= (1 << pin);
            P3SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT4:
        switch (mux)
        {
        case MUXA:
            P4SEL0 &= ~(1 << pin);
            P4SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P4SEL0 |= (1 << pin);
            P4SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P4SEL0 &= ~(1 << pin);
            P4SEL1 |= (1 << pin);
            break;
        case MUXD:
            P4SEL0 |= (1 << pin);
            P4SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT5:
        switch (mux)
        {
        case MUXA:
            P5SEL0 &= ~(1 << pin);
            P5SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P5SEL0 |= (1 << pin);
            P5SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P5SEL0 &= ~(1 << pin);
            P5SEL1 |= (1 << pin);
            break;
        case MUXD:
            P5SEL0 |= (1 << pin);
            P5SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT6:
        switch (mux)
        {
        case MUXA:
            P6SEL0 &= ~(1 << pin);
            P6SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P6SEL0 |= (1 << pin);
            P6SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P6SEL0 &= ~(1 << pin);
            P6SEL1 |= (1 << pin);
            break;
        case MUXD:
            P6SEL0 |= (1 << pin);
            P6SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT7:
        switch (mux)
        {
        case MUXA:
            P7SEL0 &= ~(1 << pin);
            P7SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P7SEL0 |= (1 << pin);
            P7SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P7SEL0 &= ~(1 << pin);
            P7SEL1 |= (1 << pin);
            break;
        case MUXD:
            P7SEL0 |= (1 << pin);
            P7SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT8:
        switch (mux)
        {
        case MUXA:
            P8SEL0 &= ~(1 << pin);
            P8SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P8SEL0 |= (1 << pin);
            P8SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P8SEL0 &= ~(1 << pin);
            P8SEL1 |= (1 << pin);
            break;
        case MUXD:
            P8SEL0 |= (1 << pin);
            P8SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT9:
        switch (mux)
        {
        case MUXA:
            P9SEL0 &= ~(1 << pin);
            P9SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P9SEL0 |= (1 << pin);
            P9SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P9SEL0 &= ~(1 << pin);
            P9SEL1 |= (1 << pin);
            break;
        case MUXD:
            P9SEL0 |= (1 << pin);
            P9SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    case PORT10:
        switch (mux)
        {
        case MUXA:
            P10SEL0 &= ~(1 << pin);
            P10SEL1 &= ~(1 << pin);
            break;
        case MUXB:
            P10SEL0 |= (1 << pin);
            P10SEL1 &= ~(1 << pin);
            break;
        case MUXC:
            P10SEL0 &= ~(1 << pin);
            P10SEL1 |= (1 << pin);
            break;
        case MUXD:
            P10SEL0 |= (1 << pin);
            P10SEL1 |= (1 << pin);
            break;
        default:
            break;
        }
        break;

    default:
        // TODO implement the other GPIO ports
        break;
    }
}

void gpio_setOutput(GPIO_Port_t gpio, Pin_t pin, State_t state)
{
    switch (gpio)
    {
    case PORT1:
        if (state == HIGH)
        {
            P1OUT |= (state << pin);
        }
        else
        {
            P1OUT &= ~(state << pin);
        }
        break;
    case PORT2:
        if (state == HIGH)
        {
            P2OUT |= (1 << pin);
        }
        else
        {
            P2OUT &= ~(1 << pin);
        }
        break;
    case PORT3:
        if (state == HIGH)
        {
            P3OUT |= (1 << pin);
        }
        else
        {
            P3OUT &= ~(1 << pin);
        }
        break;
    case PORT4:
        if (state == HIGH)
        {
            P4OUT |= (1 << pin);
        }
        else
        {
            P4OUT &= ~(1 << pin);
        }
        break;
    case PORT5:
        if (state == HIGH)
        {
            P5OUT |= (1 << pin);
        }
        else
        {
            P5OUT &= ~(1 << pin);
        }
        break;
    case PORT6:
        if (state == HIGH)
        {
            P6OUT |= (1 << pin);
        }
        else
        {
            P6OUT &= ~(1 << pin);
        }
        break;
    case PORT7:
        if (state == HIGH)
        {
            P7OUT |= (1 << pin);
        }
        else
        {
            P7OUT &= ~(1 << pin);
        }
        break;
    case PORT8:
        if (state == HIGH)
        {
            P8OUT |= (1 << pin);
        }
        else
        {
            P8OUT &= ~(1 << pin);
        }
        break;
    case PORT9:
        if (state == HIGH)
        {
            P9OUT |= (1 << pin);
        }
        else
        {
            P9OUT &= ~(1 << pin);
        }
        break;
    case PORT10:
        if (state == HIGH)
        {
            P10OUT |= (1 << pin);
        }
        else
        {
            P10OUT &= ~(1 << pin);
        }
        break;
    default:
        break;
    }
}

void gpio_toggleOutput(GPIO_Port_t gpio, Pin_t pin)
{
    switch (gpio)
    {
    case PORT1:
        P1OUT ^= (1 << pin);
        break;
    case PORT9:
        P9OUT ^= (1 << pin);
        break;
    default:
        // TODO implement the other ports to toggle
        break;
    }
}

void gpio_setREN(GPIO_Port_t gpio, Pin_t pin, Resistor_t resistor)
{
    switch (gpio)
    {
    case PORT1:
        switch (resistor)
        {
        case PULLUP:
            P1REN |= (1 << pin);
            P1OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P1REN |= (1 << pin);
            P1OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT2:
        switch (resistor)
        {
        case PULLUP:
            P2REN |= (1 << pin);
            P2OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P2REN |= (1 << pin);
            P2OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT3:
        switch (resistor)
        {
        case PULLUP:
            P3REN |= (1 << pin);
            P3OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P3REN |= (1 << pin);
            P3OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT4:
        switch (resistor)
        {
        case PULLUP:
            P4REN |= (1 << pin);
            P4OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P4REN |= (1 << pin);
            P4OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT5:
        switch (resistor)
        {
        case PULLUP:
            P5REN |= (1 << pin);
            P5OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P5REN |= (1 << pin);
            P5OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT6:
        switch (resistor)
        {
        case PULLUP:
            P6REN |= (1 << pin);
            P6OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P6REN |= (1 << pin);
            P6OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT7:
        switch (resistor)
        {
        case PULLUP:
            P7REN |= (1 << pin);
            P7OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P7REN |= (1 << pin);
            P7OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT8:
        switch (resistor)
        {
        case PULLUP:
            P8REN |= (1 << pin);
            P8OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P8REN |= (1 << pin);
            P8OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT9:
        switch (resistor)
        {
        case PULLUP:
            P9REN |= (1 << pin);
            P9OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P9REN |= (1 << pin);
            P9OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    case PORT10:
        switch (resistor)
        {
        case PULLUP:
            P10REN |= (1 << pin);
            P10OUT |= (1 << pin);
            break;
        case PULLDOWN:
            P10REN |= (1 << pin);
            P10OUT &= ~(1 << pin);
            break;
        default:
            // TODO implement further ports
            break;
        }
        break;
    default:
        // TODO implement further ports
        break;
    }
}

void gpio_setInterruptEdgeSelct(GPIO_Port_t gpio, Pin_t pin, Edge_t edge)
{
//TODO check implementation
    switch (gpio)
    {
    case PORT1:
        switch (edge)
        {
        case FALLING_EDGE:
            P1IES |= (1 << pin);
            break;
        case RISING_EDGE:
            P1IES &= ~(1 << pin);
            break;
        default:
            break;
        }
        break;
    case PORT3:
        switch (edge)
        {
        case FALLING_EDGE:
            P1IES |= (1 << pin);
            break;
        case RISING_EDGE:
            P1IES &= ~(1 << pin);
            break;
        default:
            break;
        }
    case PORT4:
        switch (edge)
        {
        case FALLING_EDGE:
            P1IES |= (1 << pin);
            break;
        case RISING_EDGE:
            P1IES &= ~(1 << pin);
            break;
        default:
            break;
        }
    default:
        // TODO implement further interrupt edge select
        break;
    }
}

void gpio_toggleInterruptEdgeSelect(GPIO_Port_t gpio, Pin_t pin)
{
    switch (gpio)
    {
    case PORT1:
        P1IES ^= (1 << pin);
        break;
    case PORT2:
        P2IES ^= (1 << pin);
        break;
    case PORT3:
        P3IES ^= (1 << pin);
        break;
    case PORT4:
        P4IES ^= (1 << pin);
        break;

    default:
        // TODO implement further interrupt edge select
        break;
    }
}

void gpio_enableInterrupt(GPIO_Port_t gpio, Pin_t pin, Interrupt_t state)
{
    switch (gpio)
    {
    case PORT1:
        switch (state)
        {
        case ENABLE_INTERRUPT:
            P1IE |= (1 << pin);
            break;
        case DISABLE_INTERRUPT:
            P1IE &= ~(1 << pin);
            break;
        default:
            break;
        }
        break;
    case PORT3:
        switch (state)
        {
        case ENABLE_INTERRUPT:
            P1IE |= (1 << pin);
            break;
        case DISABLE_INTERRUPT:
            P1IE &= ~(1 << pin);
            break;
        default:
            break;
        }
        break;
    case PORT4:
        switch (state)
        {
        case ENABLE_INTERRUPT:
            P1IE |= (1 << PIN_BUTTON_S2);
            break;
        case DISABLE_INTERRUPT:
            P1IE &= ~(1 << PIN_BUTTON_S2);
            break;
        default:
            break;
        }
        break;

    default:
        // TODO implement further enableInterrupt cases
        break;
    }
}

void gpio_clearInterruptFlag(GPIO_Port_t gpio, Pin_t pin)
{
    switch (gpio)
    {
    case PORT3:
        P1IFG &= ~(1 << pin);
        break;
    case PORT4:
        P1IFG &= ~(1 << pin);
        break;
    default:
        // TODO implement further ports
        break;
    }
}

void gpio_init()
{
// ULP Advisor > Rule 4.1 Terminate unused GPIOs
// Initialize all ports to default state
// http://processors.wiki.ti.com/index.php/Compiler/diagnostic_messages/MSP430/10372

    PADIR |= (BIT_16_MASK);
    PBDIR |= (BIT_16_MASK);
    PCDIR |= (BIT_16_MASK);
    PDDIR |= (BIT_16_MASK);
    PEDIR |= (BIT_16_MASK);
    P1DIR |= (BIT_16_MASK);
    P2DIR |= (BIT_16_MASK);
    P3DIR |= (BIT_16_MASK);
    P4DIR |= (BIT_16_MASK);
    P5DIR |= (BIT_16_MASK);
    P6DIR |= (BIT_16_MASK);
    P7DIR |= (BIT_16_MASK);
    P8DIR |= (BIT_16_MASK);
    P9DIR |= (BIT_16_MASK);

    PAOUT &= ~(BIT_16_MASK);
    PBOUT &= ~(BIT_16_MASK);
    PCOUT &= ~(BIT_16_MASK);
    PDOUT &= ~(BIT_16_MASK);
    PEOUT &= ~(BIT_16_MASK);
    P1OUT &= ~(BIT_16_MASK);
    P2OUT &= ~(BIT_16_MASK);
    P3OUT &= ~(BIT_16_MASK);
    P4OUT &= ~(BIT_16_MASK);
    P5OUT &= ~(BIT_16_MASK);
    P6OUT &= ~(BIT_16_MASK);
    P7OUT &= ~(BIT_16_MASK);
    P8OUT &= ~(BIT_16_MASK);
    P9OUT &= ~(BIT_16_MASK);
}

