/*
 * i2c.c
 *
 *  Created on: 12 Dec 2018
 *      Author: Johannes
 */

#include "../hal.h"

// TODO I2C is not working poperly

#define I2C_ADDRESS_LENGTH (7)
#define I2C_REVEIVER_MODE (0)
#define I2C_TRANSMITTER_MODE (1)
#define I2C_TRANSMIT_START (1)
#define I2C_FCSL (10)
#define BIT_MASK_HIGH_BYTE (0xFF00)
#define BIT_MASK_LOW_BYTE (0xFF)
#define BIT_SHIFT_8 (8)

int i2c_init(I2C_t bus, uint8_t slaveAddr)
{
    int i = -1;
    switch (bus)
    {
    case I2C_0:
        gpio_setPinMux(I2C_0_PORT, I2C_0_SDA_PIN, MUXB);
        gpio_setPinMux(I2C_0_PORT, I2C_0_SCL_PIN, MUXB);
        UCB0CTLW0 |= UCSWRST;                     // put eUSCI_B in reset state
        UCB0CTLW0 |= UCMODE_3 | UCMST | UCSYNC | UCSSEL__SMCLK;
        UCB0BRW = I2C_FCSL;                      // baud rate = SMCLK / 8
        UCB0CTLW1 |= UCASTP_2;
        i2c_setSlaveAddress(slaveAddr);
        UCB0CTLW0 &= ~UCSWRST;                   // eUSCI_B in operational state
        UCB0IE &= ~UCTXIE;
        UCB0IE &= ~UCRXIE;
        i = 0;
        break;
    default:
        i = -1;
        break;
    }
    return i;

}

int i2c_writeByte(I2C_t bus, uint8_t slaveAddr, uint8_t writeValue)
{
    int i = -1;
    switch (bus)
    {
    case I2C_0:
        i2c_setMode(TRANSMIT);
        i2c_setSlaveAddress(slaveAddr);
        i2c_MasterSendStart();                  // Send start
        while (!(i2c_txBufIsEmpty(bus)));        // Wait for tx interrupt flag
        UCB0TXBUF = writeValue;                // Send data byte
        while (!(i2c_txBufIsEmpty(bus)));        // Wait for tx interrupt flag
        i2c_MasterSendStop();                  // Send stop
        i2c_setMode(RECEIVE);                  // Change to receive
        i2c_waitForStop();                    // Ensure stop condition got sent
        i = 0;
        break;

    default:
        i = -1;
        break;
    }
    return i;
}

int i2c_writeWord(I2C_t bus, uint8_t slaveAddr, uint16_t writeValue)
{
    int i = -1;
    switch (bus)
    {
    case I2C_0:
        i2c_setMode(TRANSMIT);
        i2c_setSlaveAddress(slaveAddr);
        i2c_MasterSendStart();                        // Send start
        while (!(i2c_txBufIsEmpty(bus)));            // Wait for tx interrupt flag
        UCB0TXBUF = ((writeValue & BIT_MASK_HIGH_BYTE) >> BIT_SHIFT_8);   // Send data byte
        while (!(i2c_txBufIsEmpty(bus)));            // Wait for tx interrupt flag
        UCB0TXBUF = (writeValue & BIT_MASK_LOW_BYTE);
        while (!(i2c_txBufIsEmpty(bus)));
        i2c_MasterSendStop();                      // Send stop
        i2c_setMode(RECEIVE);                         // Change to receive
        i2c_waitForStop();               // Ensure stop condition got sent
        i = 0;
        break;

    default:
        i = -1;
        break;
    }
    return i;
}

int i2c_readByte(I2C_t bus, uint8_t slaveAddr, uint8_t *outValue)
{
    int i = -1;
    switch (bus)
    {
    case I2C_0:
        i2c_MasterSendStart();
        i2c_waitForStart();
        while (!(i2c_rxHasByte(I2C_0)));
        *outValue = UCB0RXBUF;
        while (!(i2c_rxHasByte(I2C_0)));
        i2c_waitForStop();
        break;
    default:
        i = -1;
        break;
    }
    return i;
}

int i2c_readWord(I2C_t bus, uint8_t regAddr, uint16_t *outValue)
{
    {
        int i = -1;
        switch (bus)
        {
        case I2C_0:
            i2c_MasterSendStart();
            i2c_waitForStart();
            while (!(i2c_rxHasByte(I2C_0)));
            *outValue = (UCB0RXBUF << BIT_SHIFT_8);
            while (!(i2c_rxHasByte(I2C_0)));
            *outValue = (*outValue | UCB0RXBUF);
            i2c_waitForStop();
            break;
        default:
            i = -1;
            break;
        }
        return i;
    }
}

void i2c_setSlaveAddress(uint8_t slaveAddress)
{
    UCB0I2CSA = slaveAddress;
}

void i2c_setMode(Mode_t i2cMode)
{
    switch (i2cMode)
    {
    case TRANSMIT:
        UCB0CTLW0 |= UCTR;
        break;
    case RECEIVE:
        UCB0CTLW0 &= ~UCTR;
        break;
    default:
        break;
    }
}

void i2c_MasterSendStop()
{
    UCB0CTLW0 |= UCTXSTP;
}

void i2c_MasterSendStart()
{
    UCB0CTL1 |= UCTXSTT;
}

bool i2c_txBufIsEmpty(I2C_t bus)
{
    bool value = false;
    switch (bus)
    {
    case I2C_0:
        value = (UCB0IFG & UCTXIFG);
        break;
    default:
        break;
    }
    return value;
}

bool i2c_rxHasByte(I2C_t bus)
{
    bool value = false;
    switch (bus)
    {
    case I2C_0:
        value = (UCB0IFG & UCRXIFG);
        break;
    default:
        break;
    }
    return value;
}

void i2c_waitForStop()
{
    while (UCB0CTLW0 & UCTXSTP);
}

void i2c_waitForStart()
{
    while (UCB0CTLW0 & UCTXSTT);
}
