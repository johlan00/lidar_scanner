/*
 * interrupt.c
 *
 *  Created on: Nov 10, 2018
 *      Author: Johannes
 */

#include "../interrupt.h"

void interrupt_enableGlobalInterrupt(){
    __enable_interrupt();
}
