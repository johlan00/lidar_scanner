/*
 * UART.h
 *
 *  Created on: Nov 28, 2018
 *      Author: Johannes
 */

#ifndef HAL_MSP430_UART_H_
#define HAL_MSP430_UART_H_

#include "hal.h"

typedef enum{
    UART0,
    UART1
}UART_t;

void uart_init(UART_t uart);
void uart_writeByte(UART_t uart, const uint8_t byte);
void uart_writeString(UART_t uart, const uint8_t str[]);
void uart_readByte(UART_t uart, uint8_t *outValue);
void uart_readString(UART_t uart, uint8_t *outValue);
void uart_writeWord(UART_t uart, const uint16_t word);
bool uart_txBufIsEmpty(UART_t uart);
bool uart_rxHasByte(UART_t uart);

#endif /* HAL_MSP430_UART_H_ */
