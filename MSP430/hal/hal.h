/*
 * hal.h
 *
 *  Created on: 27 Oct 2018
 *      Author: Johannes
 */

#ifndef HAL_HAL_H_
#define HAL_HAL_H_

#include <stdio.h>
#include <inttypes.h>
#include <stdbool.h>
#include "board/board.h"
#include "interrupt.h"
#include "timer.h"
#include "uart.h"
#include "../system/system.h"
#include "gpio.h"
#include "i2c.h"

#endif /* HAL_HAL_H_ */
