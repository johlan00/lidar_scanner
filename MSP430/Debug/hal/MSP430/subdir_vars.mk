################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../hal/MSP430/GPIO.c \
../hal/MSP430/UART.c \
../hal/MSP430/i2c.c \
../hal/MSP430/interrupt.c \
../hal/MSP430/timer.c 

C_DEPS += \
./hal/MSP430/GPIO.d \
./hal/MSP430/UART.d \
./hal/MSP430/i2c.d \
./hal/MSP430/interrupt.d \
./hal/MSP430/timer.d 

OBJS += \
./hal/MSP430/GPIO.obj \
./hal/MSP430/UART.obj \
./hal/MSP430/i2c.obj \
./hal/MSP430/interrupt.obj \
./hal/MSP430/timer.obj 

OBJS__QUOTED += \
"hal/MSP430/GPIO.obj" \
"hal/MSP430/UART.obj" \
"hal/MSP430/i2c.obj" \
"hal/MSP430/interrupt.obj" \
"hal/MSP430/timer.obj" 

C_DEPS__QUOTED += \
"hal/MSP430/GPIO.d" \
"hal/MSP430/UART.d" \
"hal/MSP430/i2c.d" \
"hal/MSP430/interrupt.d" \
"hal/MSP430/timer.d" 

C_SRCS__QUOTED += \
"../hal/MSP430/GPIO.c" \
"../hal/MSP430/UART.c" \
"../hal/MSP430/i2c.c" \
"../hal/MSP430/interrupt.c" \
"../hal/MSP430/timer.c" 


