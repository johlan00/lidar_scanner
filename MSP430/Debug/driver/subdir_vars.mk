################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driver/TFmini.c \
../driver/button.c \
../driver/led.c \
../driver/servo.c \
../driver/timer.c \
../driver/uartDriver.c 

C_DEPS += \
./driver/TFmini.d \
./driver/button.d \
./driver/led.d \
./driver/servo.d \
./driver/timer.d \
./driver/uartDriver.d 

OBJS += \
./driver/TFmini.obj \
./driver/button.obj \
./driver/led.obj \
./driver/servo.obj \
./driver/timer.obj \
./driver/uartDriver.obj 

OBJS__QUOTED += \
"driver/TFmini.obj" \
"driver/button.obj" \
"driver/led.obj" \
"driver/servo.obj" \
"driver/timer.obj" \
"driver/uartDriver.obj" 

C_DEPS__QUOTED += \
"driver/TFmini.d" \
"driver/button.d" \
"driver/led.d" \
"driver/servo.d" \
"driver/timer.d" \
"driver/uartDriver.d" 

C_SRCS__QUOTED += \
"../driver/TFmini.c" \
"../driver/button.c" \
"../driver/led.c" \
"../driver/servo.c" \
"../driver/timer.c" \
"../driver/uartDriver.c" 


