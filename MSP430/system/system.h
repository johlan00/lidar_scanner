/*
 * system.h
 *
 *  Created on: 27 Oct 2018
 *      Author: Johannes
 */

#ifndef HAL_SYSTEM_H_
#define HAL_SYSTEM_H_

#include "../hal/hal.h"
#include "../hal/GPIO.h"

void system_systickInit(void);
uint32_t system_getSysticks(void);
void system_sleep(uint32_t millis);

#endif /* HAL_SYSTEM_H_ */
