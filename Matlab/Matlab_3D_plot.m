%% 3D Plot
% Michael Maetzler
% Johannes Lang
% Fachhochschule Vorarlberg

r = 10;                                   %Kugelradius

coordinates = zeros(18*18+1,3);           %Array fuerr kartesische Koordinaten

figure(1);
view(0, 15);
hold on

c = 1;                                    %counter

for phi=0:10:180                          %phi
    for theta=-90:10:90                   %theta
         
        [x,y,z] = calcCartesian(r,phi,theta);    
        coordinates(1,c) = x;
        coordinates(2,c) = y;
        coordinates(3,c) = z;
        c = c + 1;
    end
end


scatter3(coordinates(1,:), coordinates(2,:), coordinates(3,:),1);

%%render surface
figure(2)
tri = delaunay(coordinates(1,:),coordinates(2,:));
trisurf(tri, coordinates(1,:), coordinates(2,:), coordinates(3,:));
axis vis3d
lighting phong
shading interp



