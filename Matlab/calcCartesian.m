%% 3D Plot
% Michael Maetzler
% Johannes Lang
% Fachhochschule Vorarlberg

function [x,y,z] = calcCartesian(r,phi,theta)
      x = r*cosd(phi)*sind(theta);
      y = r*sind(phi)*sind(theta);
      z = r*cosd(theta);
end