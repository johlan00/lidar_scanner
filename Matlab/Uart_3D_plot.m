%% 3D Plot
% Michael Maetzler
% Johannes Lang
% Fachhochschule Vorarlberg

close all
clear clc
coordinates = [0,0,0,0];
count = 0;
delete_points_percent = 0.01;

if ~isempty(instrfind)      %delete all ports
     fclose(instrfind);
      delete(instrfind);
end


s = serial('/dev/tty.usbmodem14203'); % Windows: serial('COM6'); 
set(s,'BaudRate',9600);
set(s, 'Timeout', 36000);        %1h
flushinput(s);

fopen(s);

boolean startbyte;
startbyte = false;
boolean delete_first_point;
delete_first_point = true;
x = 1;


%figure(1);
figure('units','normalized','outerposition',[0 0 1 1])
axis equal
view(0, 0);
%hold on
tic;

while(1)

      data = str2double(fgets(s));

 
      if(data == 11)           %Abbruchbedingung
         break; 
      end
      


if(startbyte == true)
       
       if(x==1)
           r_t = data;
       
       elseif(x==2)
               r_h = data;
               
       elseif(x==3)
               r_z = data;
               
       elseif(x==4)
               r_e = data;
               
       elseif(x==5)
               theta_h = data;
               
       elseif(x==6)
               theta_z = data;
               
       elseif(x==7)
               theta_e = data;
               
       elseif(x==8)
               phi_h = data;
               
       elseif(x==9)
               phi_z = data;
               
       elseif(x==10)
               phi_e = data;
               
               r = r_t*1000 + r_h*100 + r_z*10 + r_e;
               phi = phi_h*100 + phi_z*10 + phi_e;
               theta = theta_h*100 + theta_z*10 + theta_e;
          
               [x,y,z] = calcCartesian(r,phi,theta);
               coordinates = [coordinates;[x,y,z,r]];
               

%                disp(['distance: ', num2str(r)])
%                disp(['phi: ', num2str(phi)])
%                disp(['theta: ', num2str(theta)])
               

%% liveplot

               if(toc > 10)   
               scatter3(coordinates(:,1), coordinates(:,2), coordinates(:,3),[],coordinates(:,2),'filled');
               colorbar
               drawnow %limitrate
               tic;
               end
%
               startbyte = false;
       end
       
       x = x+1;
       
end
                                                                                   
                                                                                  

      if(data == 10)           % Startbyte
          startbyte = true;
          x = 1;
      end

end

coordinates = coordinates(2:end,:);
fclose(s);


%% Daten "aufbereiten"

coordinates_sort = sortrows(coordinates,4);

delete_points = (size(coordinates_sort,1)*delete_points_percent)/2;
coordinates_sort = coordinates_sort(delete_points:end-delete_points,:);


scatter3(coordinates_sort(:,1), coordinates_sort(:,2), coordinates_sort(:,3),[],coordinates_sort(:,4),'filled');
colorbar
%hold off

%%render surface

figure('units','normalized','outerposition',[0 0 1 1])
tri = delaunay(coordinates_sort(:,1),coordinates_sort(:,2));
trimesh(tri, coordinates_sort(:,1), coordinates_sort(:,2), coordinates_sort(:,3));
axis equal
axis vis3d
lighting phong
shading interp
